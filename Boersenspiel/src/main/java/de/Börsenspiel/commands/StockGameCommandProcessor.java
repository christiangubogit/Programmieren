package de.Börsenspiel.commands;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import de.Börsenspiel.AccountManager.IAccountManager;
import de.Börsenspiel.Exceptions.Meldung;

/**
 * Kommandprozessor liest die Befehle ein und ruft Scanner und Descriptor auf um den Befehl zu erkennen.
 * Wenn der Befehl erkannt wurde wird die jeweilige Methode im IAccountManager aufgerufen
 * 
 * @author Christian Gubo
 * @version 4
 */

public class StockGameCommandProcessor {

	private final IAccountManager IAM;
	private final BufferedReader shellReader;
	private final PrintWriter shellWriter;

	public StockGameCommandProcessor(IAccountManager IAM) {
		this.IAM = IAM;
		shellReader = new BufferedReader(new InputStreamReader(System.in));
		shellWriter = new PrintWriter(System.out, true);
		shellWriter
				.println("Welcome! \n <help> - for list of commands \n\n Have Fun and Good Trading!");
	}

	public void exit() {
		shellWriter.println("Spiel wird beendet");
		System.exit(0);
	}

	public void help() {
		for (int i = 0; i < StockGameCommandType.values().length; i++) {
			shellWriter.println(StockGameCommandType.values()[i].getCommand()
					+ StockGameCommandType.values()[i].getDescription());
		}
	}

	public void process() {
		while (true) {
			try {
				final String[] input = shellReader.readLine().split(" ");

				final CommandScanner scanner = new CommandScanner(
						StockGameCommandType.values(), input);
				final CommandDescriptor cd = new CommandDescriptor();
				scanner.sendtodescriptor(cd);
				final StockGameCommandType commandType = (StockGameCommandType) cd
						.getCommandType();
				final Object[] params = cd.getParams();

				if (commandType.getMethodname() != null) {

					final Class<IAccountManager> c = IAccountManager.class;
					final Method method = c.getMethod(cd.getName(),
							commandType.getParamTypes());
					method.invoke(IAM, params);
				} else if (commandType.getCommand() != null) {

					if (commandType.getCommand() == "exit") {
						exit();
					}
					if (commandType.getCommand() == "help") {
						help();
					}
				} else {
					shellWriter.println("Befehl wurde nicht erkannt");
					new Meldung("Befehl wurde nicht erkannt");
				}
			} catch (NoSuchMethodException | SecurityException | IOException
					| IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				e.printStackTrace();
				shellWriter.println(e.getCause().getCause().getCause()
						.getMessage());
			} catch (final Throwable e) {
				e.printStackTrace();
			} finally {

			}
		}
	}
}
