package de.Börsenspiel.commands;

import java.io.IOException;
/**
 * Sucht das vom User eingegebene Kommando. Anschließend Speichert es das Kommando und die Parameter im CommandDescripor ab
 * 
 * @author Christian Gubo
 * @version 1.0
 */
public class CommandScanner {
	String[] input;
	StockGameCommandType[] values;

	public CommandScanner(StockGameCommandType[] values, String[] input) {
		this.input = input;
		this.values = values;
	}

	public void sendtodescriptor(CommandDescriptor cd) throws IOException {
		final Object[] params = new Object[input.length - 1];

		if (input.length == 1) {
			for (final StockGameCommandType value : values) {
				if (value.getCommand().equals(input[0])) {
					cd.setCommandType(value);
					cd.setParams(values.getClass().getClasses());
					for (int j = 1; j < input.length; j++) {
						params[j - 1] = input[j];
					}

					cd.setParams(params);
					cd.setName(value.getMethodname());
					break;
				}
			}
		}

		if (input.length >= 2) {
			for (final StockGameCommandType value : values) {
				if (value.getCommand().split(" ")[0].equals(input[0])) {
					cd.setCommandType(value);
					for (int j = 1; j < input.length; j++) {
						params[j - 1] = input[j];
					}
					if (input.length == 4) {
						params[2] = Integer.parseInt(input[3]);
					}
					cd.setParams(params);
					cd.setName(value.getMethodname());
					break;
				}
			}
		}
	}
}