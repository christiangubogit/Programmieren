package de.Börsenspiel.commands;

public enum StockGameCommandType implements ICommandTypeInfo {

	AUTOMODE("automode",
			" > Computer plays for Player \n      -auto <playername>",
			"automode", String.class), BOT("bot", " > create a BOT",
			"createBot"), BUY("buy",
			" > Buy Shares \n       -buy <playername> <sharename> <amount>",
			"buyShare", String.class, String.class, int.class), CREATE(
			"create", " > create a new Player \n       -create <playername>",
			"createPlayer", String.class), EXIT("exit", " > exit the game",
			null), HELP("help", " > list all commands", null), KONTOSORT(
			"konto", " > get Konto \n       -konto <playername> <sorttype>",
			"kontosort", String.class, String.class), PLAYER("player",
			"> get the Assets of a Player \n       -player <playername>",
			"playerToString", String.class), SELL("sell",
			" > Sell Shares \n       -sell <playername> <sharename> <amount>",
			"sellShare", String.class, String.class, int.class), START("start",
			" > start the game", "startUpdate");

	private final String command;
	private final String description;
	private final String methodname;
	private final Class<?>[] params;

	StockGameCommandType(String command, String description, String name,
			Class<?>... params) {
		this.command = command;
		this.description = description;
		methodname = name;
		this.params = params;
	}

	@Override
	public String getCommand() {
		return command;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getMethodname() {
		return methodname;
	}

	@Override
	public Class<?>[] getParamTypes() {
		return params;
	}

}
