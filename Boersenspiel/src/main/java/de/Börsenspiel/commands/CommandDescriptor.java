package de.Börsenspiel.commands;
/**
 * Speichert das Aktuelle Kommando mit allen Parametern zwischen
 * 
 * @author Christian Gubo
 * @version 1.0
 */
public class CommandDescriptor {

	private String name;
	private Object[] params;
	private ICommandTypeInfo type;

	public CommandDescriptor() {

	}

	public ICommandTypeInfo getCommandType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public Object[] getParams() {
		return params;
	}

	public void setCommandType(ICommandTypeInfo type) {
		this.type = type;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setParams(Object[] params) {
		this.params = params;
	}

}
