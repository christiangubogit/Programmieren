package de.B�rsenspiel.commands;

public interface ICommandTypeInfo {
	/**
	 * gibt das Kommando zur�ck
	 * @return String 		Kommando als String
	 */
	String getCommand();

	/**
	 * Gibt Befehlsbeschreibung zur�ck als hilfe f�r den User 
	 * @return String 		Beschreibung als String
	 */
	String getDescription();

	/**
	 * gibt den Methodenname zur�ck der im IAccountManager aufgerufen werden muss
	 * @return String		Methodenname als String
	 */
	String getMethodname();

	/**
	 * Gibt Liste der Parameter eines Befehls zur�ck
	 * @return Class[]...		Liste aller Parameter
	 */
	Class<?>[] getParamTypes();
}
