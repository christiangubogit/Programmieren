package de.B�rsenspiel.B�rse;
/**
 * Hier werden f�r jede Aktie die komplette Preisentwicklung gespeichert
 * @author Christian Gubo
 * @version 1.0
 *
 */
public class priceHistory {
	private int counter;
	private long[] history;
	private long last;

	public priceHistory(long price) {
		history = new long[1];
		history[0] = price;
		counter = 1;
	}

	public long getLast() {
		return last;
	}

	/**
	 * Gibt den durschnittspreis der Aktie von den letzten zwei Preisaktualisierungen zur�ck
	 * @return long		Preis
	 */
	public long getLAST() {
		long buffer = 0;
		if (history.length < 2) {
			return history[0];
		}
		buffer = history[history.length - 1]
				+ (history[history.length - 2] / 2);
		return buffer;
	}

	/**
	 * gibt den Durschnittspreis der Aktie �ber den kompletten Zeitraum zur�ck
	 * @return long		Preis
	 */
	public long getValue() {
		long buffer = 0;
		for (final long element : history) {
			buffer += element;
		}
		buffer = buffer / counter;

		return buffer;
	}

	/**
	 * Nach jeder Preisaktualisierung wird das Array um 1 Feld vergr��ert und der neue Wert wird eingespeichert
	 * @param price 		neuer Wert einer Aktie
	 */
	public void update(long price) {
		final long[] buffer = new long[history.length + 1];
		for (int i = 0; i < history.length; i++) {
			buffer[i] = history[i];
		}
		buffer[history.length] = price;

		history = buffer;
		counter += 1;
	}

}
