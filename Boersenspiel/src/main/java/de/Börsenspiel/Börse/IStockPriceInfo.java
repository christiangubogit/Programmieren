package de.B�rsenspiel.B�rse;

import de.B�rsenspiel.Exceptions.ShareNotFound;
/**
 * Interface des StockPriceProviders. Dieser Enth�lt eine Aktienliste und verwaltet alle Aktionen der Aktien
 * 
 * @author Christian Gubo
 * @version 1.0
 */
public interface IStockPriceInfo {
	/**
	 * Momentaner Wert einer Aktie ausgeben
	 * 
	 * @param name		Aktienname 
	 * @throws ShareNotFound		Falls die Aktie nicht gefunden wird
	 */
	void getcurrentShareValue(String name) throws ShareNotFound;

	/**
	 * Gibt Liste der Shares zur�ck
	 * 
	 * @return String		String mit allen Aktien die im Spiel Sind
	 */
	String getShareList();

	/**
	 * gibt zur�ck ob eine gesuchte Aktie im Spiel ist
	 * @param name		Aktienname
	 */
	void isShareListed(String name);

	/**
	 * gibt Kopie des Aktienarrays zur�ck
	 * 
	 * @return Share[]		Kopie des Aktienarrays
	 */
	Share[] snapshot();

	/**
	 * Startet die Updateroutine der Shares
	 */
	void startUpdate();

	/**
	 * Update der Aktienpreise
	 */
	void UpdateShareRates();
}