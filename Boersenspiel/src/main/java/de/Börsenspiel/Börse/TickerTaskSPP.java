package de.B�rsenspiel.B�rse;

import java.util.TimerTask;
/**
 * TickerTask f�r den StockPriceProvider zum updaten der AktienPreise
 * @author Christian Gubo
 * @version 1.0
 */
public class TickerTaskSPP extends TimerTask {
	private final IStockPriceInfo info;

	public TickerTaskSPP(IStockPriceInfo info) {
		this.info = info;
	}

	@Override
	public void run() {
		info.UpdateShareRates();
	}

}
