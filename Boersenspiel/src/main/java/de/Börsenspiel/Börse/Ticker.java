package de.B�rsenspiel.B�rse;

import java.util.Timer;
/**
 * Erstellung einer TickerInstance, die f�r alle Zeitabh�nigen Processe im Programm benutzt wird
 * 
 * @author Christian Gubo
 * @version 1.0
 */
public class Ticker extends Timer {
	private static Ticker firstInstance = null;

	public static Ticker getInstance() {
		if (firstInstance == null) {
			firstInstance = new Ticker();
		}
		return firstInstance;
	}

	private Ticker() {
	}

}
