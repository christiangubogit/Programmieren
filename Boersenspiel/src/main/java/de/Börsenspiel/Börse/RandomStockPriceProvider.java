package de.B�rsenspiel.B�rse;

import java.util.Random;
/**
 * Zufallsgenerator f�r Aktienpreise
 * 
 * @author Christian Gubo
 * @version 1.0
 */
public class RandomStockPriceProvider extends StockPriceProvider {
	Random generator = new Random();

	@Override
	public void UpdateShareRate(Share share) {
		long newPrice;
		if (generator.nextBoolean()) {
			newPrice = share.getPrice() + generator.nextInt(10);
		} else {
			newPrice = share.getPrice() - generator.nextInt(10);
			if (newPrice < 0) {
				newPrice = newPrice * -1;
			}
			if (newPrice == 0) {
				newPrice = newPrice + 1;
			}
		}

		share.setPrice(newPrice);
		share.getPH().update(newPrice);
	}

}