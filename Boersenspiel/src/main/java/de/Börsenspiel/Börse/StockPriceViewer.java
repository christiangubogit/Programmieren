package de.Börsenspiel.Börse;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JLabel;
/**
 * Gibt in einer GUI alle Aktien mit Namen und Preis aus
 * 
 * @author Christian Gubo
 * @version 1.0
 */
public class StockPriceViewer extends JFrame {

	private class TickerTask extends TimerTask {
		private String createText() {

			String output = "<html><body> WILLKOMMEN BEIM BÖRSENSPIEL <br> <br>";
			final Calendar cal = Calendar.getInstance();
			final Date date = cal.getTime();
			final DateFormat dateFormatter = DateFormat.getDateTimeInstance();
			output += SnapshotToString(info.snapshot());
			output += "\n\n\n" + dateFormatter.format(date) + "</body></html>";

			return output;
		}

		@Override
		public void run() {
			final String output = createText();
			clockLabel.setText(output);
			clockLabel.repaint();
		}

		private String SnapshotToString(Share[] targetArray) {
			final StringBuilder sBuilder = new StringBuilder();
			for (final Share element : targetArray) {
				sBuilder.append("<br>  Sharename: ");
				sBuilder.append(element.getName());
				sBuilder.append("<br>");
				sBuilder.append("Price: ");
				sBuilder.append(element.getPrice());
				sBuilder.append("<br> <br> <br> ");
			}

			return sBuilder.toString();
		}
	}

	private static final long serialVersionUID = 1L;
	private final JLabel clockLabel;
	private final IStockPriceInfo info;

	private final TickerTask task;

	public StockPriceViewer(IStockPriceInfo info) {
		setAlwaysOnTop(true);
		task = new TickerTask();
		this.info = info;
		clockLabel = new JLabel("Daten der Aktienbörse werden abgerufen...");
		this.add("Center", clockLabel);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(200, 600);
		setVisible(true);
		final Ticker clock = Ticker.getInstance();
		clock.scheduleAtFixedRate(new TickerTask() {
		}, 1000, 500);

	}

}
