package de.B�rsenspiel.B�rse;
/**
 * Aktienobjecte mit einem Namen und einem Preis
 * 
 * @author Christian Gubo
 * @version 1.0
 */
public class Share implements Comparable<Share> {

	private String name;
	private priceHistory ph;
	private long price;

	/**
	 * @deprecated
	 */
	public Share() {

	}

	/**
	 * Standart Constuctor
	 * 
	 * @param name		Aktienname
	 * @param price		Aktienpreis
	 */
	public Share(String name, long price) {
		this.name = name;
		this.price = price;
		ph = new priceHistory(price);
	}

	@Override
	public int compareTo(Share o) {
		return name.compareTo(o.getName());
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}

		if (this.getClass() != other.getClass()) {
			return false;
		}

		final Share buffer = (Share) other;
		if (name.equals(buffer.name)) {
			return true;

		}
		return false;
	}

	public String getName() {
		return name;
	}

	public priceHistory getPH() {
		return ph;
	}

	/*
	 * Methoden
	 */

	/*
	 * Getter und Setter
	 */
	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "\nAktienname: " + name + ", Aktienpreis: " + price;
	}
}
