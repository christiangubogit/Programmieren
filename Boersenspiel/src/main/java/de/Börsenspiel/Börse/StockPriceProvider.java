package de.B�rsenspiel.B�rse;

import java.util.Set;
import java.util.TreeSet;

import de.B�rsenspiel.Exceptions.ShareNotFound;
/**
 * Implementierung des IStockPriceInfo
 * 
 * @author Christian Gubo
 * @version 1.0
 */
public abstract class StockPriceProvider implements IStockPriceInfo {
	/*
	 * Variablen
	 */
	private final Set<Share> list;
	private Share[] sharelist;

	StockPriceProvider() {
		list = new TreeSet<Share>();

		final Share Porsche = new Share("Porsche", 30L);
		final Share Audi = new Share("Audi", 60L);
		final Share Siemens = new Share("Siemens", 40L);
		final Share Samsung = new Share("Samsung", 80L);
		final Share Apple = new Share("Apple", 90L);
		final Share Microsoft = new Share("Microsoft", 100L);

		list.add(Porsche);
		list.add(Audi);
		list.add(Siemens);
		list.add(Samsung);
		list.add(Apple);
		list.add(Microsoft);

	}

	@Override
	public void getcurrentShareValue(String name) throws ShareNotFound {
		final Share buffer = searchShare(name);
		System.out.print(buffer.toString());

	}

	/*
	 * Getter und Setter
	 */
	public Share[] getList() {
		sharelist = new Share[list.size() - 1];

		return list.toArray(sharelist);
	}

	public Share getShare(String name) throws ShareNotFound {
		final Share buffer = searchShare(name);
		return buffer;
	}

	@Override
	public String getShareList() {
		final StringBuilder sBuilder = new StringBuilder();
		for (int i = 0; i < list.size(); i++) {
			sBuilder.append("<br>  Sharename: ");
			sBuilder.append(getList()[i].getName());
			sBuilder.append("<br>");
			sBuilder.append("Price: ");
			sBuilder.append(getList()[i].getPrice());
			sBuilder.append("<br> <br> <br> ");
		}

		return sBuilder.toString();
	}

	@Override
	public void isShareListed(String name) {
		// boolean returnen
	}

	/*
	 * Private Methoden
	 */
	public Share searchShare(String name) throws ShareNotFound {
		for (final Share i : list) {
			if (i.getName().equals(name)) {
				return i;
			}
		}
		throw new ShareNotFound(name + " nicht gefunden!!!!!");
	}

	@Override
	public Share[] snapshot() {
		int counter = 0;
		final Share[] targetArray = new Share[list.size()];
		for (Share i : list) {
			Share buffer = new Share(i.getName(), i.getPrice());
			targetArray[counter] = buffer;
			counter++;
		}
		return targetArray;
	}

	@Override
	public void startUpdate() {
		final Ticker clock = Ticker.getInstance();
		clock.scheduleAtFixedRate(new TickerTaskSPP(this), 1000, 1000);

	}

	abstract void UpdateShareRate(Share share);

	@Override
	public void UpdateShareRates() {
		for (int i = 0; i < list.size(); i++) {
			UpdateShareRate(getList()[i]);
		}

	}

}
