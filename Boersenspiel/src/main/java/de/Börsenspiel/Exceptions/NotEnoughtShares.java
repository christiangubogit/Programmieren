package de.Börsenspiel.Exceptions;

public class NotEnoughtShares extends Exception {

	private static final long serialVersionUID = 1L;

	public NotEnoughtShares() {
		super();
	}

	public NotEnoughtShares(String s) {
		super(s);
	}
}
