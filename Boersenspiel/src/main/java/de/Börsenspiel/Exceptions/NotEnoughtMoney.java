package de.Börsenspiel.Exceptions;

public class NotEnoughtMoney extends Exception {

	private static final long serialVersionUID = 1L;

	public NotEnoughtMoney() {
		super();
	}

	public NotEnoughtMoney(String s) {
		super(s);
		s = "Spieler besitzt nicht genügend Geld! ";
	}
}
