package de.Börsenspiel.Exceptions;

public class ShareNotFound extends ObjectNotFound {

	private static final long serialVersionUID = 1L;

	public ShareNotFound() {
		super();
	}

	public ShareNotFound(String s) {
		super(s);
	}
}
