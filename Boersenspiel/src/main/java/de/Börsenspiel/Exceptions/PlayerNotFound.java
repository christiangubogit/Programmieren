package de.Börsenspiel.Exceptions;

public class PlayerNotFound extends ObjectNotFound {

	private static final long serialVersionUID = 1L;

	public PlayerNotFound() {
		super();
	}

	public PlayerNotFound(String s) {
		super(s);
	}
}
