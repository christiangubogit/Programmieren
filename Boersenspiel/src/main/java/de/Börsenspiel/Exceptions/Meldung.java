package de.Börsenspiel.Exceptions;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Meldung extends JFrame {
	public Meldung(String s) {

		// Aufruf der statischen Methode showConfirmDialog()
		final JPanel panel = new JPanel();
		JOptionPane.showMessageDialog(panel, s, "Error",
				JOptionPane.ERROR_MESSAGE);
		setAlwaysOnTop(true);

	}
}
