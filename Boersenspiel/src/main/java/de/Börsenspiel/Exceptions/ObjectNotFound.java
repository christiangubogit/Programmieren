package de.Börsenspiel.Exceptions;

public class ObjectNotFound extends Exception {

	private static final long serialVersionUID = 1L;

	public ObjectNotFound() {
		super();
	}

	public ObjectNotFound(String s) {
		super(s);
	}
}
