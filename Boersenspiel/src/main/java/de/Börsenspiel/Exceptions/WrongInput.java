package de.Börsenspiel.Exceptions;

public class WrongInput extends Exception {

	private static final long serialVersionUID = 1L;

	public WrongInput() {
		super();
	}

	public WrongInput(String s) {
		super(s);
	}
}
