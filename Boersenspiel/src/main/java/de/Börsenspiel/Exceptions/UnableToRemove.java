package de.Börsenspiel.Exceptions;

public class UnableToRemove extends Exception {

	private static final long serialVersionUID = 1L;

	public UnableToRemove() {
		super();
	}

	public UnableToRemove(String s) {
		super(s);
	}
}
