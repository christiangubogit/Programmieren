package de.Börsenspiel.Main;

import de.Börsenspiel.AccountManager.AccountManagerImpl;
import de.Börsenspiel.AccountManager.AccountManagerProxy;
import de.Börsenspiel.AccountManager.AssetViewer;
import de.Börsenspiel.AccountManager.IAccountManager;
import de.Börsenspiel.Börse.RandomStockPriceProvider;
import de.Börsenspiel.Börse.StockPriceProvider;
import de.Börsenspiel.Börse.StockPriceViewer;
import de.Börsenspiel.commands.StockGameCommandProcessor;
/**
 * Börsenspiel der Hochschule Augsburg im 2.Semester des Informatik Studiengangs
 * 
 * @author Christian Gubo
 * @author Joshua Hörmann
 * @version Beschde
 */
public class Börsenspiel {

	public static void main(String[] args) {
		final StockPriceProvider SPP = new RandomStockPriceProvider();
		final IAccountManager IAM = new AccountManagerImpl(SPP);
		final IAccountManager proxy = AccountManagerProxy.getACCProxy(IAM);
		new AssetViewer(IAM);
		final StockGameCommandProcessor SG = new StockGameCommandProcessor(
				proxy);
		new StockPriceViewer(SPP);
		SG.process();
	}
}
