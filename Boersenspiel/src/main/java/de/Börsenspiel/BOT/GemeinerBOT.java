package de.Börsenspiel.BOT;

import java.util.Random;
import java.util.TimerTask;

import de.Börsenspiel.AccountManager.AccountManagerImpl;
import de.Börsenspiel.AccountManager.Player;
import de.Börsenspiel.AccountManager.Shareitem;
import de.Börsenspiel.Börse.Share;
import de.Börsenspiel.Börse.StockPriceProvider;
import de.Börsenspiel.Börse.Ticker;
import de.Börsenspiel.Exceptions.NotEnoughtMoney;
import de.Börsenspiel.Exceptions.PlayerNotFound;
import de.Börsenspiel.Exceptions.ShareNotFound;
import de.Börsenspiel.Exceptions.UnableToRemove;
/**
 * BOT der nur gewinnbringend Aktien kauft und verkauft
 * 
 * @author Christian Gubo
 * @version 1.0
 * @deprecated
 */
public class GemeinerBOT extends Player {
	private final AccountManagerImpl AMI;
	private final Share[] list;
	private final StockPriceProvider SPP;

	public GemeinerBOT(StockPriceProvider SPP, AccountManagerImpl AMI) {
		super("BOT");
		this.SPP = SPP;
		this.AMI = AMI;
		final Share[] buffer = SPP.getList();
		list = buffer;
		startBOT();
	}

	private void process() {
		final Random random = new Random();
		final int randomINT = random.nextInt(SPP.getList().length);
		final Share wanthave = SPP.getList()[randomINT];
		if (random.nextInt(10) > 4) {
			// BOT BUY

			if (wanthave.getPrice() < list[randomINT].getPrice()) {
				try {
					AMI.buyShare("BOT", wanthave.getName(), random.nextInt(4));
				} catch (ShareNotFound | PlayerNotFound | NotEnoughtMoney e) {
					System.out.println("money");
				}
			}
		} else {
			// BOT SELL
			try {
				final Shareitem i = AMI.shareDepositAccountToString("BOT")
						.getDepot2().get(wanthave.getName());
				if (i.getShare().getPrice() > SPP.searchShare(
						wanthave.getName()).getPrice()) {
					AMI.sellShare("BOT", wanthave.getName(), random.nextInt(4));
				}
			} catch (ShareNotFound | PlayerNotFound | UnableToRemove e) {
				System.out.print("Share");
			}
		}
	}

	private void startBOT() {
		final Ticker clock = Ticker.getInstance();
		clock.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				GemeinerBOT.this.process();
			}
		}, 1000, 1000);
	}
}
