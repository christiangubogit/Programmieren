package de.Börsenspiel.BOT;

import java.util.Random;
import java.util.TimerTask;

import de.Börsenspiel.AccountManager.IAccountManager;
import de.Börsenspiel.AccountManager.Player;
import de.Börsenspiel.AccountManager.Shareitem;
import de.Börsenspiel.Börse.Share;
import de.Börsenspiel.Börse.Ticker;
import de.Börsenspiel.Exceptions.NotEnoughtMoney;
import de.Börsenspiel.Exceptions.PlayerNotFound;
import de.Börsenspiel.Exceptions.ShareNotFound;
import de.Börsenspiel.Exceptions.UnableToRemove;
/**
 * BOT der nur gewinnbringend Aktien kauft und verkauft
 * 
 * @author Christian Gubo
 * @version 1.0
 */
public class StandartBOT {
	private final IAccountManager IAM;
	boolean onoff1;
	private final Player p;

	public StandartBOT(Player p, IAccountManager IAM) {
		if (onoff1) {
			// this.
		}

		onoff1 = true;
		this.p = p;
		this.IAM = IAM;
		final Ticker clock = Ticker.getInstance();
		clock.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				try {
					StandartBOT.this.process();
				} catch (ShareNotFound | PlayerNotFound | NotEnoughtMoney
						| UnableToRemove e) {
				}
			}
		}, 1000, 1000);
	}

	private void process() throws ShareNotFound, PlayerNotFound,
			NotEnoughtMoney, UnableToRemove {
		final Random gen = new Random();
		final int randomINT = gen.nextInt(IAM.getSPP().getList().length);
		Share share = IAM.getSPP().getList()[randomINT];
		int randomAMOUNT = gen.nextInt(5);
		if (randomAMOUNT == 0) {
			randomAMOUNT = 1;
		}
		if (gen.nextBoolean()) {
			if (share.getPH().getLAST() >= share.getPrice()) {
				IAM.buyShare(p.getName(), share.getName(), randomAMOUNT);
			}
		} else {
			for (final Shareitem s : p.getDepot().snapshot()) {
				share = s.getShare();
				if (share.getPrice() < share.getPH().getLAST()) {
					if (randomAMOUNT > s.getAmount()) {
						randomAMOUNT = s.getAmount();
					}
					IAM.sellShare(p.getName(), share.getName(), randomAMOUNT);

				}
			}
		}
	}
}
