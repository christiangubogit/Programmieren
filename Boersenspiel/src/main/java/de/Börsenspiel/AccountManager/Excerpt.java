package de.Börsenspiel.AccountManager;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
/**
 * Kontoauszüge, mit dieser Klasse wird jede Verkauf und Kauf Operationen der Spieler festgehalten und kann zu einem Späteren Zeitpunkt ausgegeben werden
 * 
 * @author Christian Gubo
 * @version 1.0
 */
public class Excerpt implements Comparable<Excerpt> {

	private final int amount;
	private final Date date;
	private final String name;
	private final String opperation;
	private final String playername;
	private final long price;

	public Excerpt(String name, String playername, long price, int amount,
			String opperation) {
		super();

		final Calendar cal = Calendar.getInstance();
		final Date date = cal.getTime();
		this.playername = playername;
		this.name = name;
		this.price = price;
		this.amount = amount;
		this.opperation = opperation;
		this.date = date;
	}

	@Override
	public int compareTo(Excerpt arg0) {
		if (date.getTime() > arg0.getDate().getTime()) {
			return 0;
		} else {
			return 1;
		}
	}

	public int getAmount() {
		return amount;
	}

	public Date getDate() {
		return date;
	}

	public String getName() {
		return name;
	}

	public String getOpperation() {
		return opperation;
	}

	public long getPrice() {
		return price;
	}

	@Override
	public String toString() {
		final DateFormat dateFormatter = DateFormat.getDateTimeInstance();

		return dateFormatter.format(date) + ": " + playername + " "
				+ opperation + " " + amount + ". " + name
				+ "aktien zum Preis von " + price + "\n";
	}

}
