package de.B�rsenspiel.AccountManager;

import de.B�rsenspiel.Exceptions.NotEnoughtMoney;
/**
 * Konto f�r Spieler. Hier wird ein Geldbetrag verwalten mit dem die Akteure Aktionen ausf�hren k�nnen
 * 
 * @author Christian Gubo
 * @version 1.0
 */
public class Bankaccount extends Asset {

	private long currentMoney;

	public Bankaccount(long startmoney) {
		currentMoney = startmoney;
	}

	public void addValue(long value) {
		currentMoney += value;
	}

	public long getMoney() {
		return currentMoney;
	}

/**
 * Reduzierung des Geldbetrags auf dem Konto
 * 
 * @param value		Betrag der Abgezogen werden soll
 * @throws NotEnoughtMoney		Falls nicht mehr gen�gend Geld auf dem Konto ist
 */	
	public void removeValue(long value) throws NotEnoughtMoney {
		if ((currentMoney - value) < 0) {
			throw new NotEnoughtMoney("Sie besitzen nicht genug Geld!");
		} else {
			currentMoney -= value;
		}
	}

	@Override
	public String toString() {
		return "-Kontostand: " + currentMoney + " in Cent";
	}

}
