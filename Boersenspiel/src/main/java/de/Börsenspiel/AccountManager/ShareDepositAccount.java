package de.B�rsenspiel.AccountManager;

import java.util.HashMap;
import java.util.Map;
import java.util.TimerTask;

import de.B�rsenspiel.B�rse.Share;
import de.B�rsenspiel.B�rse.Ticker;
import de.B�rsenspiel.Exceptions.UnableToRemove;
/**
 * AKTIENDEPOT
 * Ein Aktienspeicher, hier werden Shareitems abgespeichert und k�nnen zu jedem Zeitpunkt ver�ndert werden
 * 
 * @author Christian Gubo
 * @version 1.0
 */
public class ShareDepositAccount extends Asset {

	private final Map<String, Shareitem> depot2 = new HashMap<String, Shareitem>();

	/**
	 * �berpr�fung ob ggf. Shares schon vorhanden sind, falls nicht wird ein neues Shareitem erstellt und gespeichert.
	 * Falls Shares schon vorhanden sind wird die Menge des Shareobjectes angepasst
	 * 
	 * @param share			Aktie die gespeichert werden soll
	 * @param amount		Menge der Aktien
	 */
	public void add(Share share, int amount) {
		if (depot2.containsKey(share.getName())) {
			final Shareitem buffer = depot2.get(share.getName());
			buffer.changeAmount(buffer, share, amount);
			depot2.put(buffer.getName(), buffer);
			return;
		}
		depot2.put(share.getName(), new Shareitem(share, amount));
	}

	public Map<String, Shareitem> getDepot2() {
		return depot2;
	}

	public int getLength() {
		return depot2.size();
	}

	/**
	 * Entfernen von Aktien die im Depot liegen.
	 * Falls mehr Aktien vorhanden sind als Entfernt werden sollen, wird das Shareitem angepasst
	 * Falls weniger Aktien vorhanden sind UnableToRemoveException  wird ausgegebn
	 * Falls genau gleich viele vorhanden sind, wie entfernt werden sollen, wird das Object gel�scht
	 * 
	 * @param share			Aktie die gekauft werden soll
	 * @param amount		Menge der zu verkaufenden Aktien
	 * @throws UnableToRemove		Falls weniger Aktien vorhanden sind als verkauft werden sollen
	 */
	public void remove(Share share, int amount) throws UnableToRemove {
		final Shareitem s = depot2.get(share.getName());
		boolean b = false;
		if (s.getShare().getName().equals(share.getName())) {
			if (s.getAmount() < amount) {
				throw new UnableToRemove("> Sie besitzen nicht genug Aktien");
			} else if (s.getAmount() == amount) {
				depot2.remove(share.getName());
				b = true;
			} else if (s.getAmount() > amount) {
				s.changeAmount(s, share, -amount);
				b = true;
			}
		}

		if (!b) {
			throw new UnableToRemove("> Sie besitzen keine Aktien von "
					+ share.getName());
		}
	}

	/**
	 * @return Shareitem[]		gibt Array aller Shares im Depot zur�ck
	 */
	public Shareitem[] snapshot() {
		final Shareitem[] result = new Shareitem[depot2.values().size()];
		int counter = 0;

		for (final Shareitem i : depot2.values()) {
			result[counter] = new Shareitem(i.getShare(), i.getAmount());
			counter++;
		}

		return result;
	}

	@Override
	public String toString() {
		final StringBuilder b = new StringBuilder();
		b.append("Aktiendepot: \n");
		for (final Shareitem i : depot2.values()) {
			b.append(i.toString());
		}

		b.append("\n Gesamtwert: " + value);
		return b.toString();
	}

	/**
	 * Gesamtwert der Depots wird geupdatet
	 */
	private void update() {
		value = 0;
		depot2.forEach((String a, Shareitem s) -> value += s.getShare()
				.getPrice() * s.getAmount());
	}

	/**
	 * Updatemethode  f�r die Preise der Aktien die im Depot liegen
	 * @param list
	 * @deprecated
	 */
	public void update(Share[] list) {
		for (int i = 0; i < depot2.size(); i++) {
			for (final Shareitem s : depot2.values()) {
				for (final Share element : list) {
					if (s.getName().equals(element)) {
						s.changeAmount(s, element, 0);
					}
				}
			}
		}

	}

	/*
	 * Methoden
	 */
	public void updateDepotValue() {
		final Ticker clock = Ticker.getInstance();
		clock.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				ShareDepositAccount.this.update();
			}
		}, 1000, 1000);
	}
}

// Set<Shareitem> result = new TreeSet<Shareitem>();
// depot2.forEach((String s, Shareitem i) -> { result.add( new
// Shareitem(i.getShare(), i.getAmount()));});
// return (Shareitem[]) result.toArray();
