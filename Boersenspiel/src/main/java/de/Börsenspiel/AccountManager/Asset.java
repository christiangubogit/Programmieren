package de.B�rsenspiel.AccountManager;
/**
 * 
 * @author Christian Gubo
 * @version 1.0
 */
public abstract class Asset {
	String name;
	long value;

	/**
	 * Standartconstructor
	 */
	Asset() {

	}

	/**
	 * Dieser Construktor sollte Standartm��ig verwendet werden um Spielerassets zu erstellen
	 * 
	 * @param name		Assetname, wird f�r die Unterklassen vererbt
	 * @param value		AssetValue, wird f�r die Unterklassen vererbt
	 */
	Asset(String name, long value) {
		this.name = name;
		this.value = value;
	}

	/*
	 * Getter und Setter
	 */
	String getName() {
		return name;
	}

	long getValue() {
		return value;
	}

	void setName(String name) {
		this.name = name;
	}

	/*
	 * Methoden
	 */

	@Override
	public String toString() {
		return "\n\n Aktienpaket: \n\n   " + "von " + name + ",   Wert: "
				+ value;
	}

}
