package de.B�rsenspiel.AccountManager;

import de.B�rsenspiel.B�rse.Share;

public class Shareitem extends Asset {

	private int amount;
	private long averagePrice;
	private long purchaseValue;
	/*
	 * Variablen
	 */
	private Share share;

	/*
	 * Constructor
	 */
	public Shareitem() {

	}

	public Shareitem(Share share, int amount) {
		this.share = share;
		this.amount = amount;
		purchaseValue = share.getPrice() * amount;
		name = share.getName() + "aktien";
		value = purchaseValue;
		averagePrice = purchaseValue / amount;
		// Werte neu setzten
	}

	public void changeAmount(Shareitem item, Share share, int aamount) {

		this.share = share;
		purchaseValue = item.purchaseValue + (share.getPrice() * aamount);
		amount = item.amount + aamount;
		averagePrice = purchaseValue / amount;

	}

	public int getAmount() {
		return amount;
	}

	public long getAveragePrice() {
		return averagePrice;
	}

	/*
	 * Methoden
	 */

	/*
	 * Getter und Setter
	 */
	public long getPurchaseValue() {
		return purchaseValue;
	}

	public Share getShare() {
		return share;
	}

	@Override
	public String toString() {
		return share + ", Paketkaufpreis: " + purchaseValue + ", Menge: "
				+ amount + ", Durschnittlicherkaufpreis: " + averagePrice
				+ "|||";
	}

}
