package de.Börsenspiel.AccountManager.ComperatorKonto;

import java.util.Comparator;

import de.Börsenspiel.AccountManager.Excerpt;
/**
 * Comparator der Kontoauszüge nach der Zeit sortiert
 * Default Comparator
 * 
 * @author Christian Gubo
 * @version 1.0
 */
public class ExcerptTimeComperator implements Comparator<Excerpt> {

	@Override
	public int compare(Excerpt o1, Excerpt o2) {
		if (o1.getDate().getTime() > o2.getDate().getTime()) {
			return 1;
		} else if (o1.getDate().getTime() == o2.getDate().getTime()) {
			return 0;
		} else {
			return -1;
		}
	}

}
