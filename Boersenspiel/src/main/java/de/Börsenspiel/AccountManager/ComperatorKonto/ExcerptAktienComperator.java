package de.Börsenspiel.AccountManager.ComperatorKonto;

import java.util.Comparator;

import de.Börsenspiel.AccountManager.Excerpt;
/**
 * Comperator der Kontoauszüge nach Aktiennamen sortiert
 * 
 * @author Christian Gubo
 * @version 1.0
 */
public class ExcerptAktienComperator implements Comparator<Excerpt> {

	@Override
	public int compare(Excerpt o1, Excerpt o2) {
		if (o1.getName().compareTo(o2.getName()) < 0) {
			return -1;
		} else if (o1.getName().compareTo(o2.getName()) == 0) {
			return 0;
		} else {
			return 1;
		}
	}

}
