package de.B�rsenspiel.AccountManager;

import java.util.Arrays;
import java.util.Random;
import java.util.TimerTask;

import de.B�rsenspiel.BOT.StandartBOT;
import de.B�rsenspiel.B�rse.Share;
import de.B�rsenspiel.B�rse.StockPriceProvider;
import de.B�rsenspiel.B�rse.Ticker;
import de.B�rsenspiel.Exceptions.NotEnoughtMoney;
import de.B�rsenspiel.Exceptions.PlayerAlreadyExists;
import de.B�rsenspiel.Exceptions.PlayerNotFound;
import de.B�rsenspiel.Exceptions.ShareNotFound;
import de.B�rsenspiel.Exceptions.UnableToRemove;
/**
 * 
 * @author Christian Gubo
 * @version 1.0
 */
public class AccountManagerImpl implements IAccountManager {

	
	private Player currentPlayer;

	private Player[] player = new Player[0];
	private final StockPriceProvider SPP;

	/** StandartConstruktor ben�tigt immer einen StockPriceProvider
	 * 
	 * @param SPP		StockPriceProvider
	 */
	public AccountManagerImpl(StockPriceProvider SPP) {
		this.SPP = SPP;

	}

	
	/**
	 * Klasse zum zuf�lligen Kauf under verkauf von Akiten f�r einen Spieler wird von der Klasse <b> automode()<b\> aufgerufen
	 * 
	 * 
	 * @param name		Spielername f�r den der Automode angesetzt wird
	 * @throws PlayerNotFound		Falls Spieler nicht gefunden wird
	 * @throws ShareNotFound		Falls Share nicht gefunden werden kann
	 * @throws NotEnoughtMoney		Auf dem Konto des Spielers befindet sich nicht gen�gend Geld um gen�gend Aktien zu kaufen
	 * @throws UnableToRemove		Beim Verkauf einer Aktie ist ein Fehler aufgetreten
	 */
	private void auto(String name) throws PlayerNotFound, ShareNotFound,
			NotEnoughtMoney, UnableToRemove {
		final Random gen = new Random();
		final Player current = searchPlayer(name);
		final int randomINT = gen.nextInt(SPP.getList().length);
		Share share = SPP.getList()[randomINT];
		int randomAMOUNT = gen.nextInt(5);
		if (randomAMOUNT == 0) {
			randomAMOUNT = 1;
		}
		if (gen.nextBoolean()) {
			if (share.getPH().getLAST() >= share.getPrice()) {
				buyShare(name, share.getName(), randomAMOUNT);
			}
		} else {
			for (final Shareitem s : current.getDepot().getDepot2().values()) {
				share = s.getShare();
				if (share.getPrice() < share.getPH().getLAST()) {
					final Shareitem buffer = current.getDepot().getDepot2()
							.get(share.getName());
					if (randomAMOUNT > buffer.getAmount()) {
						randomAMOUNT = buffer.getAmount();
					}
					sellShare(name, share.getName(), randomAMOUNT);

				}
			}
		}
	}


	@Override
	public void automode(final String name) {
		final Ticker clock = Ticker.getInstance();
		clock.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				try {
					auto(name);
				} catch (PlayerNotFound | ShareNotFound | NotEnoughtMoney
						| UnableToRemove e) {

				}
			}
		}, 1000, 1000);

	}

	@Override
	public void buyShare(String name, String share, int amount)

			throws NotEnoughtMoney, ShareNotFound, PlayerNotFound {
		currentPlayer = searchPlayer(name);
		final Share buffer = SPP.getShare(share);
		currentPlayer.buy(buffer, amount);

	}

	@Override
	public void createBot() throws PlayerAlreadyExists {
		try {
			searchPlayer("BOT");
		} catch (final PlayerNotFound e) {

			final Player[] buffer = new Player[player.length + 1];
			for (int i = 0; i < player.length; i++) {
				buffer[i] = player[i];
			}
			player = buffer;
			player[player.length - 1] = new Player("BOT");
			new StandartBOT(player[player.length - 1], this);
			return;
		}

		throw new PlayerAlreadyExists("BOT wurde bereits erstellt");

	}

	@Override
	public void createPlayer(String name) throws PlayerAlreadyExists {
		try {
			searchPlayer(name);
		} catch (final PlayerNotFound e) {
			final Player[] buffer = new Player[player.length + 1];
			for (int i = 0; i < player.length; i++) {
				buffer[i] = player[i];
			}
			player = buffer;

			player[player.length - 1] = new Player(name);
			return;
		}
		throw new PlayerAlreadyExists("Spieler " + name + " existiert bereits");
	}

	@Override
	public String getcurrentShareValue(String name) throws ShareNotFound {
		final Share buffer = SPP.getShare(name);

		return buffer.toString();
	}

	@Override
	public Bankaccount getOneKonto(String name) throws PlayerNotFound {
		currentPlayer = searchPlayer(name);

		return currentPlayer.getKonto();

	}

	@Override
	public Player[] getPlayers() {
		return player;
	}

	@Override
	public void getShareList() {
		Arrays.toString(SPP.getList());
	}


	@Override
	public StockPriceProvider getSPP() {
		return SPP;
	}

	@Override
	public void kontosort(String name, String sort) throws PlayerNotFound {
		currentPlayer = searchPlayer(name);
		System.out.println(currentPlayer.konto(sort));
	}


	@Override
	public String PlayerSnapshot() {
		final StringBuilder builder = new StringBuilder();
		for (final Player element : player) {
			builder.append(element.getName() + ": \n\n Depot : \n\r");
			builder.append(element.getDepot().toString() + "\n\r");

			builder.append(element.getKonto().toString() + "\n\n\n\r\r\r");
		}

		return builder.toString();
	}

	@Override
	public String playerToString(String name) throws PlayerNotFound {
		final String buffer = searchPlayer(name).toString(SPP.getList());
		return buffer;
	}

	private Player searchPlayer(String name) throws PlayerNotFound {
		for (final Player element : player) {
			if (element.getName().equals(name)) {
				return element;
			}
		}
		throw new PlayerNotFound(name + " nicht gefunden!!!!!");
	}

	@Override
	public void sellShare(String name, String share, int amount)
			throws ShareNotFound, PlayerNotFound, UnableToRemove {
		currentPlayer = searchPlayer(name);
		final Share buffer = SPP.getShare(share);
		currentPlayer.sell(buffer, amount);

	}

	@Override
	public ShareDepositAccount shareDepositAccountToString(String name)
			throws PlayerNotFound {
		currentPlayer = searchPlayer(name);
		return currentPlayer.getDepot();
	}

	@Override
	public void startUpdate() {
		SPP.startUpdate();
	}

	@Override
	public String toString() {
		return "AccountManagerImpl [player=" + Arrays.toString(player)
				+ ", list=" + Arrays.toString(SPP.getList())
				+ ", currentPlayer=" + currentPlayer + ", playercounter=" + "]";
	}

}
