package de.B�rsenspiel.AccountManager;

import de.B�rsenspiel.B�rse.StockPriceProvider;
import de.B�rsenspiel.Exceptions.NotEnoughtMoney;
import de.B�rsenspiel.Exceptions.PlayerAlreadyExists;
import de.B�rsenspiel.Exceptions.PlayerNotFound;
import de.B�rsenspiel.Exceptions.ShareNotFound;
import de.B�rsenspiel.Exceptions.UnableToRemove;

public interface IAccountManager {
	/**
	 * Automodus Klasse um einen Spieler autmatisch Aktien kaufen und Verkaufen zu lassen
	 * 
	 * @param name		Spielername f�r den der Automodus aktiviert werden soll
	 * @throws PlayerNotFound		Falls Spieler nicht gefunden wird
	 * @throws ShareNotFound		Falls Share nicht gefunden werden kann
	 * @throws NotEnoughtMoney		Auf dem Konto des Spielers befindet sich nicht gen�gend Geld um gen�gend Aktien zu kaufen
	 * @throws UnableToRemove		Beim Verkauf einer Aktie ist ein Fehler aufgetreten
	 */
	void automode(String name) throws PlayerNotFound, ShareNotFound,
			NotEnoughtMoney, UnableToRemove;

	/**
	 * Kaufe Aktien f�r einen Spieler
	 * 
	 * @param name		Spielername f�r den die Aktien gekauft werden sollen
	 * @param share		Aktienname welche gekauft werden soll
	 * @param amount	Menge der zu kaufenden Aktien
	 * @throws PlayerNotFound		Falls Spieler nicht gefunden wird
	 * @throws ShareNotFound		Falls Share nicht gefunden werden kann
	 * @throws NotEnoughtMoney		Auf dem Konto des Spielers befindet sich nicht gen�gend Geld um gen�gend Aktien zu kaufen
	 */
	void buyShare(String name, String share, int amount)
			throws NotEnoughtMoney, ShareNotFound, PlayerNotFound;

	/**
	 *  Bot erstellen der automatisch spielt
	 */
	void createBot() throws PlayerAlreadyExists;

	/**
	 * Einen neuen Spieler erstellen
	 * 
	 * @param name		Spielername f�r den neuen Spieler
	 * @throws PlayerAlreadyExists		Wenn ein Spieler mit diesem Namen bereits erstellt wurde
	 */
	void createPlayer(String name) throws PlayerAlreadyExists;

	/**
	 * @param name		Sharename
	 * @throws ShareNotFound		Wenn das Share nicht im Spiel gefunden worden ist
	 */
	String getcurrentShareValue(String name) throws ShareNotFound;

	/**
	 * Konto eines Spielers ausgeben
	 * 
	 * @param name		Spielername
	 * @throws PlayerNotFound		Spieler wurde nicht gefunden in der Spielerliste
	 */
	Bankaccount getOneKonto(String name) throws PlayerNotFound;

	/**
	 * Spielerliste ausgeben
	 */
	Player[] getPlayers();


	/**
	 * Alle Shares ausgeben
	 */
	void getShareList();

	/**
	 * Aktueller StockPriceProvider wird zur�ckgegeben
	 * @return StockPriceProvider		AktienSystem wird zur�ckgegeben
	 */
	StockPriceProvider getSPP();

	/**
	 * Kontoauszug eines Spielers mit allen Kaufpreisen etc.
	 * 
	 * @param name		Spielername
	 * @param sort		Sortiereigenschaft
	 * @throws PlayerNotFound		Spieler  wurde in der SPielerliste nicht gefunden
	 */
	void kontosort(String name, String sort) throws PlayerNotFound;

	/**
	 * 
	 * @return String		String mit allen Spielerdaten
	 */
	String PlayerSnapshot();

	/**
	 * 
	 * @param name		Name eines Aktiven Spielers
	 * @return String		Konto und Depot des Spielers werden als String zur�ckgegeben
	 * @throws PlayerNotFound		Name des Spielers wurde nicht gefunden	
	 */
	String playerToString(String name) throws PlayerNotFound;

	/**
	 * Verkaufen einer Aktie eines Spielers
	 * 
	 * @param name		Spielername f�r den die Aktien gekauft werden sollen
	 * @param share		Aktienname welche gekauft werden soll
	 * @param amount	Menge der zu kaufenden Aktien
	 * @throws PlayerNotFound		Falls Spieler nicht gefunden wird
	 * @throws ShareNotFound		Falls Share nicht gefunden werden kann
	 * @throws UnableToRemove		Beim Verkauf der Aktien ist ein Fehler aufgetreten
	 */
	void sellShare(String name, String share, int amount) throws ShareNotFound,
			PlayerNotFound, UnableToRemove;

	/**
	 * 
	 * @param name			Spielername des Gesuchten Spielers
	 * @return String		Depot eines Spielers wird als String zur�ckgegeben
	 * @throws PlayerNotFound		Spielername wurde nicht in der Liste der Aktiven Spieler gefunden
	 */
	ShareDepositAccount shareDepositAccountToString(String name)
			throws PlayerNotFound;

	/**
	 * Spielstart der Aktienpreisaktualisierung
	 */
	void startUpdate();

}
