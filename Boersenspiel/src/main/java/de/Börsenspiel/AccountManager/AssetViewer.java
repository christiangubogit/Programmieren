package de.B�rsenspiel.AccountManager;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JLabel;

import de.B�rsenspiel.B�rse.Ticker;
/**
 * GUI f�r die Assets aller Spieler 
 * 
 * @author Christian Gubo
 * @version 1.0
 */
public class AssetViewer extends JFrame {

	private class TickerTask extends TimerTask {
		private String createText() {
			final Player[] player = IAM.getPlayers();
			String output = "<html><body> Spielerliste: <br> <br>";
			final Calendar cal = Calendar.getInstance();
			final Date date = cal.getTime();
			final DateFormat dateFormatter = DateFormat.getDateTimeInstance();
			for (final Player element : player) {
				output += element.getName() + ": <br>";
				output += element.getDepot().toString() + ": <br>";
				output += element.getKonto().toString() + " <br><br>";
			}
			output += "\n\n\n" + dateFormatter.format(date) + "</body></html>";

			return output;
		}

		@Override
		public void run() {
			final String output = createText();
			clockLabel.setText(output);
			clockLabel.repaint();
		}
	}

	private static final long serialVersionUID = 1L;
	private final JLabel clockLabel;
	private final IAccountManager IAM;

	public AssetViewer(IAccountManager IAM) {
		setAlwaysOnTop(true);
		this.IAM = IAM;
		clockLabel = new JLabel("Daten der Spieler werden abgerufen...");
		this.add("Center", clockLabel);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setBounds(200, 0, 400, 400);
		setVisible(true);

		final Ticker clock = Ticker.getInstance();
		clock.scheduleAtFixedRate(new TickerTask() {
		}, 1000, 1000);
	}

}
