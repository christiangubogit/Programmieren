package de.Börsenspiel.AccountManager;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import de.Börsenspiel.AccountManager.ComperatorKonto.ExcerptAktienComperator;
import de.Börsenspiel.AccountManager.ComperatorKonto.ExcerptTimeComperator;
import de.Börsenspiel.Börse.Share;
import de.Börsenspiel.Exceptions.NotEnoughtMoney;
import de.Börsenspiel.Exceptions.UnableToRemove;

/**
 * Spielerobjecte, Jeder Spieler besitzt ein Aktiendepot, Kontoauszüge und ein Geldkonto. Hier wird das Startgeld jedes Spielers festgelegt
 * 
 * @author Christian Gubo
 * @version 1.0
 */
public class Player {

	private ShareDepositAccount depot;
	private Bankaccount konto;
	private final List<Excerpt> kontoauszug = new Vector<Excerpt>();
	/*
	 * Variablen
	 */
	private String name;

	/*
	 * Constructor
	 */
	public Player() {

	}

	/**
	 * erstellt einen neuen Spieleraccount
	 * 
	 * @param name		Spielername
	 */
	public Player(String name) {
		this.name = name;
		depot = new ShareDepositAccount();
		konto = new Bankaccount(2000);

		depot.updateDepotValue();
	}

	/**
	 * Methode wird über den Accountmanager aufgerufen und gibt die Parameter direkt weiter
	 * 
	 * @param share			Sharename welches gekauft werden soll
	 * @param amount		Menge der zu kaufenden Aktien
	 * @throws NotEnoughtMoney	
	 */
	public void buy(Share share, int amount) throws NotEnoughtMoney {
		konto.removeValue(share.getPrice() * amount);
		depot.add(share, amount);
		kontoauszug.add(new Excerpt(share.getName(), name, share.getPrice(),
				amount, "buy"));
	}

	public ShareDepositAccount getDepot() {
		return depot;
	}

	public Bankaccount getKonto() {
		return konto;
	}


	public String getName() {
		return name;
	}

	
	/**
	 * Sortiert kontoauszüge eines SPielers und gibt diese aus
	 * 
	 * @param sort		Sortierart
	 * @return	String 
	 */
	public String konto(String sort) {
		switch (sort) {
		case "time":
			final Comparator<Excerpt> comb = new ExcerptTimeComperator();
			Collections.sort(kontoauszug, comb);
			break;
		case "aktie":
			final Comparator<Excerpt> comb2 = new ExcerptAktienComperator();
			Collections.sort(kontoauszug, comb2);
			break;
		default:
			final List<Excerpt> buffer = new Vector<Excerpt>();
			kontoauszug.forEach((Excerpt e) -> {
				if (e.getName() == sort) {
					buffer.add(e);
				}
			});

			final StringBuilder s = new StringBuilder();

			for (final Excerpt i : buffer) {
				s.append(i.toString());
			}

			return s.toString();
		}

		final StringBuilder s = new StringBuilder();

		for (final Excerpt i : kontoauszug) {
			s.append(i.toString());
		}

		return s.toString();

	}

	/**
	 * Methode wird von  AccountManager aufgerufen um Aktien eines Spielers zu verkaufen
	 * 
	 * @param share			Name des Shares welches gekauft werdeen soll
	 * @param amount		Menge der zu kaufenden Aktien
	 * @throws UnableToRemove
	 */
	public void sell(Share share, int amount) throws UnableToRemove {
		depot.remove(share, amount);
		konto.addValue(share.getPrice() * amount);
		kontoauszug.add(new Excerpt(share.getName(), name, share.getPrice(),
				amount, "sell"));
	}

	public String toString(Share[] list) {
		getDepot().update(list);
		return "Spielername: " + name + "\n\n Aktiendepot: " + depot
				+ "\n\n Geldkonto: " + konto;
	}
}
