package de.B�rsenspiel.AccountManager;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
/**
 * 
 * @author Christian Gubo
 * @version 1.0
 */
public final class AccountManagerProxy {
	/**
	 * Proxy der alle Aktionen der Spieler loggt. Dabei wird ein File auf dem Desktop des Benutzers erstellt.
	 * 
	 * @param s		Interface von IAccountManager f�r den der Proxy die �berwachung �bernehmen soll
	 */
	public static IAccountManager getACCProxy(final IAccountManager s) {
		final Logger logger = Logger.getLogger("LogB�rsenspiel");
		LogManager.getLogManager().addLogger(logger);
		// TODO richtig laden f�rs loggen
		try {
			LogManager
					.getLogManager()
					.readConfiguration(
							new FileInputStream(
									"C:/Users/Chris/Desktop/FH AUGSBURG/mylogging.properties"));

		} catch (SecurityException | IOException e1) {
			e1.printStackTrace();
		}

		return (IAccountManager) Proxy.newProxyInstance(s.getClass()
				.getClassLoader(), new Class[] { IAccountManager.class },
				(proxy, method, args) -> {

					Object result = null;

					switch (method.getName()) {
					case "createPlayer": {
						logger.log(Level.FINE,
								method.getName() + Arrays.toString(args));
						break;
					}
					case "createBot": {
						logger.log(Level.FINE,
								method.getName() + Arrays.toString(args));
						break;
					}
					case "automode": {
						logger.log(Level.FINE,
								method.getName() + Arrays.toString(args));
						break;
					}
					case "buyShare": {
						logger.log(Level.FINE,
								method.getName() + Arrays.toString(args));
						break;
					}
					case "sellShare": {
						logger.log(Level.FINE,
								method.getName() + Arrays.toString(args));
						break;
					}

					}
					try {
						result = method.invoke(s, args);
					} catch (IllegalAccessException | IllegalArgumentException
							| InvocationTargetException e) {
						logger.log(Level.SEVERE, e.getCause().toString());
						e.printStackTrace();
					}
					return result;
				});
	}
}
