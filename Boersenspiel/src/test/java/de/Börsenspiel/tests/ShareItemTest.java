package de.Börsenspiel.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.Börsenspiel.AccountManager.Shareitem;
import de.Börsenspiel.Börse.Share;

public class ShareItemTest {
	private int AddAmount;
	private Shareitem item;
	private int PAV;
	private int PRV;
	private int resultAddAmount;
	private int resultRemoveAmount;
	private Share share;

	@Test
	public void AmountAddTest() {
		item.changeAmount(item, share, AddAmount);
		Assert.assertTrue((item.getAmount() == resultAddAmount)
				&& (item.getPurchaseValue() == PAV));
	}

	@Test
	public void AmountRemoveTest() {
		item.changeAmount(item, share, -AddAmount);
		Assert.assertTrue((item.getAmount() == resultRemoveAmount)
				&& (item.getPurchaseValue() == PRV));
	}

	@Before
	public void setUp() throws Exception {
		share = new Share("test", 10);
		item = new Shareitem(share, 10);
		AddAmount = 5;

		resultAddAmount = 10 + AddAmount;
		resultRemoveAmount = 10 - AddAmount;
		PAV = 100 + 50;
		PRV = 100 - 50;

	}
}
