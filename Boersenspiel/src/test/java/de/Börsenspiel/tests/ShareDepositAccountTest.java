package de.Börsenspiel.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import de.Börsenspiel.AccountManager.ShareDepositAccount;
import de.Börsenspiel.Börse.Share;
import de.Börsenspiel.Exceptions.ShareNotFound;
import de.Börsenspiel.Exceptions.UnableToRemove;

public class ShareDepositAccountTest {

	private ShareDepositAccount acc;
	private int amoundAdd;
	private int amount;
	private Share test;
	private Share test2;

	
	
	 @Before
	 public void setUp() throws Exception {
	 acc = new ShareDepositAccount();
	 test = new Share("test", 10);
	 test2 = new Share("test2", 20);
	 amount = 10;
	 amoundAdd = amount + amount;
	 }
	
	 @Test
	 public void add() {
	 acc.add(test, amount);
	 Assert.assertTrue(acc.getDepot2().containsKey(test.getName()));

	
	 }
	
	 @Test
	 public void Add2() {
	 acc.add(test, amount);
	 acc.add(test2, amount);
	
	 Assert.assertTrue(acc.getDepot2().containsKey(test.getName())
	 && acc.getDepot2().containsKey(test2.getName()));
	 }
	
	 @Test
	 public void Add3() {
	 acc.add(test, amount);
	 acc.add(test, amount);
	
	 Assert.assertTrue(acc.getDepot2().containsKey(test.getName()));
	 Assert.assertTrue(acc.getDepot2().get(test.getName()).getAmount() == amoundAdd);
	 }
	
	 
	 @Test
	 public void removeERROR() {
		 try {
			acc.remove(test, amount);
			Assert.assertTrue(false);
		} catch (UnableToRemove e) {
				Assert.assertTrue(true);
		}
	 
	 
	 
	 }
	
	 @Test
	 public void removeOneCompleteOneItem() throws UnableToRemove {
	 acc.add(test, amount);
	 Assert.assertTrue(acc.getDepot2().containsKey(test.getName()));
	 acc.remove(test, amount);
	 Assert.assertTrue(acc.getDepot2().isEmpty());
	
	 }
	
	 @Test
	 public void removeOneCompleteTwoItem() throws UnableToRemove {
	 acc.add(test, amount);
	 acc.add(test2, amount);
	 Assert.assertTrue(acc.getDepot2().containsKey(test.getName()));
	 acc.remove(test, amount);
	 Assert.assertTrue(acc.getDepot2().size() == 1);
	 }
	
	 @Test
	 public void removeOnePart() throws UnableToRemove {
	 acc.add(test, amoundAdd);
	 Assert.assertTrue(acc.getDepot2().containsKey(test.getName()));
	 acc.remove(test, amount);
	 Assert.assertTrue(acc.getDepot2().get(test.getName()).getAmount() == amount);
	
	 }
	 
}
