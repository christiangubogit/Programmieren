package de.Börsenspiel.Exceptions;

public class ProviderNotFound extends Exception {

	private static final long serialVersionUID = 1L;
	public ProviderNotFound() { super();}
	public ProviderNotFound(String s) { super(s);}
}
