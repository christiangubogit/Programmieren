package de.Börsenspiel.Exceptions;

public class PlayerAlreadyExists extends Exception {

	private static final long serialVersionUID = 1L;
	public PlayerAlreadyExists() { super();}
	public PlayerAlreadyExists(String s) { super(s);}
}
