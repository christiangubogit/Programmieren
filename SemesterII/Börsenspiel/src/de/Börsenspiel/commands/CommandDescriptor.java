package de.Börsenspiel.commands;

public class CommandDescriptor {

	private ICommandTypeInfo type;
	private Object[] params;
	
	public CommandDescriptor(){
		
	}
	
	public ICommandTypeInfo getCommandType(){
		return type;
	}
	
	public void setCommandType (ICommandTypeInfo type){
		this.type = type;
	}

	public Object[] getParams() {
		return params;
	}

	public void setParams(Object[] params) {
		this.params = params;
	}
	
	
}
