package de.Börsenspiel.commands;

public interface ICommandTypeInfo {
	
	String getCommand();
	String getDescription();
	Class<?>[] getParamTypes();
}
