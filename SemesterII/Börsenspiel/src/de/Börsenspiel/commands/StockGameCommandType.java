package de.Börsenspiel.commands;

public enum StockGameCommandType implements ICommandTypeInfo {
	HELP("help", " > list all commands"),
	START("start", " > start the game"),
	CREATE("create", " > create a new Player", String.class ),
	PLAYER("player", "> get the Assets of a Player", String.class),
	BUY("buy", " > Buy Shares", String.class, String.class, int.class),
	SELL("sell", " > Sell Shares", String.class, String.class, int. class),
	EXIT("exit", " > exit the game");
	
	
	private final String command;
	private final String description;
	private final Class<?>[] params;
	
	StockGameCommandType(String command, String description, Class<?>... params){
		this.command = command;
		this.description = description;
		this.params = params;
	}

	@Override
	public String getCommand() {
		return this.command;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

	@Override
	public Class<?>[] getParamTypes() {
		return this.params;
	}
	
	
	
}
