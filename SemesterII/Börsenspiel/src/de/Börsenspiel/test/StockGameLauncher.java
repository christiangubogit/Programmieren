package de.Börsenspiel.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;







import de.Börsenspiel.AccountManager.AccountManagerImpl;
import de.Börsenspiel.AccountManager.IAccountManager;
import de.Börsenspiel.Exceptions.*;

public class StockGameLauncher {

	public static void main(String[] args) throws IOException{
		IAccountManager IA = new AccountManagerImpl("random");
		running(IA);    
	}
	
	
	
	
	
	
	public static void running(IAccountManager IA) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String s = null;
		try {
			s = reader.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String[] input = s.split(" ");
		
		if(s != null){
		while(true){
			if(input.length == 1){
				if(input[0].equals("exit")){
					System.out.print("Bye Bye!");
					System.exit(0);
				}else if(input[0].equals("help")){
					System.out.println("<playername> buy <sharename> <amount> - to buy a share for a player");
					System.out.println("<playername> sell <sharename> <amount> - to sell a share for a player");
					System.out.println("create <playername> - create a new player");
					System.out.println("player <playername> - get Playerdepot and Konto");
					System.out.println("start - start the game!");
					System.out.println();
					System.out.println("exit - exit the game");
					
				}else if(input[0].equals("start")){
					IA.getSPP().startUpdate();
				}
			}else if(input.length == 2){
				if(input[0].equals("create")){
					IA.createPlayer(input[1]);
				}else if(input[0].equals("player")){
					System.out.println(IA.playerToString(input[1]));
				}		
			}else if(input.length == 4){
				if(input[1].equals("buy")){
					IA.buyShare(input[0], input[2],Integer.parseInt(input[3]));
					
				}else if(input[1].equals("sell")){
					IA.sellShare(input[0], input[2],Integer.parseInt(input[3]));
				}
			}else{
				System.out.print("error");
			}
			
			try {
				s = reader.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			input = s.split(" ");
		}
		}	
	}
}
