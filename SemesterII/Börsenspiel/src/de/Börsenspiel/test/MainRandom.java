package de.Börsenspiel.test;

import java.util.Random;


public class MainRandom {

	
	public static void main(String[] args){
		Random generator = new Random();
		
		int vorzeichen = generator.nextInt(2);
		
		
		double multiplier = generator.nextDouble();
		long i = Math.round(multiplier*100);
		multiplier = (double) i/100;
		
		if(vorzeichen == 1){
			multiplier = -multiplier;
		}
		
		
		System.out.print(multiplier);
	}
}
