package de.B�rsenspiel.B�rse;

import java.util.Arrays;
import java.util.TimerTask;

import de.B�rsenspiel.Exceptions.ShareNotFound;


 public abstract class StockPriceProvider implements IStockPriceInfo {
	/*
	 * Variablen
	 */
	protected Share[] list = new Share[6];
	StockPriceViewer SPV;
	
	
	 StockPriceProvider(){
		
		Share Porsche = new Share("Porsche", 30L);
		Share Audi = new Share("Audi", 60L);
		Share Siemens = new Share("Siemens", 40L);
		Share Samsung = new Share("Samsung", 80L);
		Share Apple = new Share("Apple", 90L);
		Share Microsoft = new Share("Microsoft", 100L);
		
		list[0] = Porsche;
		list[1] = Audi;
		list[2] = Siemens;
		list[3] = Samsung;
		list[4] = Apple;
		list[5] = Microsoft;
		
		this.SPV = new StockPriceViewer(this);
	}
	
	/*
	 * Getter und Setter
	 */
	public Share[] getList(){
		return list;
	}
	
	public Share getShare(String name) throws ShareNotFound{
		Share buffer = searchShare(name);
		return buffer;
	}
	/*
	 * Private Methoden
	 */
	private Share searchShare(String name) throws ShareNotFound{
		for(int i = 0; i < list.length; i++){				
			if(list[i].getName().equals(name)){
				return list[i];
			}
		}
		throw new ShareNotFound(name + " nicht gefunden!!!!!");
	}

	/*
	 * Interface Methoden
	 */
	
	@Override
	public String getShareList() {
		StringBuilder sBuilder = new StringBuilder();
		for(int i = 0; i < list.length; i++){
			sBuilder.append("<br>  Sharename: ");
			sBuilder.append(this.list[i].getName());
			sBuilder.append("<br>");
			sBuilder.append("Price: " );
			sBuilder.append(this.list[i].getPrice());
			sBuilder.append("<br> <br> <br> "); 
		}
		
		return sBuilder.toString();
	}


	@Override
	public void getcurrentShareValue(String name) throws ShareNotFound {
		Share buffer = searchShare(name);
		System.out.print(buffer.toString());
		
	}
	
	@Override
	public void isShareListed(String name){
		//boolean returnen
	}
	
	public void startUpdate(){
		Ticker clock = Ticker.getInstance();		
	     clock.scheduleAtFixedRate(new TimerTask() {
	    	public void run() {
	    		UpdateShareRates();
	    		SPV.process();
	    	}
	    }, 1000, 4000);	    
	}
	
	@Override
	public Share[] snapshot(){
		Share[] sourceArray = list;
		Share[] targetArray = java.util.Arrays.copyOf( sourceArray, list.length );
		
		return targetArray;
	}
	
	@Override
	public void UpdateShareRates(){
		for(int i = 0; i < list.length; i++){
			UpdateShareRate(list[i]);
		}
	}
	
	abstract void UpdateShareRate(Share share);
	
	
	@Override
	public String toString() {
		return Arrays.toString(list);
	}

}
