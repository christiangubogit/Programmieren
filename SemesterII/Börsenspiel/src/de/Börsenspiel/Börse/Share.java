package de.B�rsenspiel.B�rse;

public class Share {
	
	/*
	 * Variablen
	 */
	private String name;
	private long price;
	
	/*
	 * Constructor f�r Share
	 */
	public Share(String name, long price){
		this.name = name;
		this.price = price;
	}
	
	public Share() {
		// TODO Auto-generated constructor stub
	}
	
	/*
	 * Getter und Setter
	 */
	public long getPrice(){
		return price;
	}
	public void setPrice(long price){
		this.price = price;
	}
	
	public String getName(){
		return name;
	}
	
	/*
	 * Methoden
	 */
	
	@Override
	public boolean equals(Object other){
		if(this == other){
			return true;
		}
		
		if(this.getClass() != other.getClass()){
			return false;
		}
		
		Share buffer = (Share) other;
		if(this.name.equals(buffer.name)){
				return true;
			
		}
		return false;
	}
	
	@Override
	public String toString(){
		return "\nAktienname: " + name + ", Aktienpreis: " + price;
	}
}
