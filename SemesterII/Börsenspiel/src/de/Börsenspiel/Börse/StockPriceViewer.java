package de.Börsenspiel.Börse;

import java.util.TimerTask;
import java.util.Calendar;
import java.util.Date;
import java.text.DateFormat;

import javax.swing.JFrame;
import javax.swing.JLabel;


public class StockPriceViewer extends JFrame {

	private static final long serialVersionUID = 1L;

    private JLabel clockLabel;
    private IStockPriceInfo info;
    private TickerTask task;
    

	private class TickerTask extends TimerTask {
        public void run() {
            String output = createText();
            clockLabel.setText(output);
            clockLabel.repaint();
        }

        private String createText() {     
        	
            String output = "<html><body> WILLKOMMEN BEIM BÖRSENSPIEL <br> <br>";
            Calendar cal = Calendar.getInstance();
            Date date = cal.getTime();
            DateFormat dateFormatter = DateFormat.getDateTimeInstance();
            output += StockPriceViewer.this.info.snapshot().toString();
            output += dateFormatter.format(date) + "</body></html>";
            
            return output;
        }
    }


    public StockPriceViewer(IStockPriceInfo info) {
    	this.task = new TickerTask();
    	this.info = info;
        clockLabel = new JLabel("Daten der Aktienbörse werden abgerufen...");
        add("Center", clockLabel);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(200, 600);
        setVisible(true);
        this.task.run();								// direkt aufrufen beim initailisieren
    }
    
    public void process(){
    	this.task.run();
    }






}

