package de.B�rsenspiel.B�rse;

import de.B�rsenspiel.Exceptions.ShareNotFound;


public interface IStockPriceInfo {
	 String getShareList();
	 void getcurrentShareValue(String name) throws ShareNotFound;
	 void isShareListed(String name);
	 void UpdateShareRates();
	 void startUpdate();
	 Share[] snapshot();
	 									//snapshot von allen Shares
	 									//clonabes f�r shares in neuer liste
}