package de.Börsenspiel.AccountManager;


public abstract class Asset {
	
	/*
	 * Variablen
	 */
	String name;
	long value;
	
	/*
	 * Constructor
	 */
	Asset(){
		
	}
	
	Asset(String name, long value){
		this.name = name;
		this.value = value;
	}
	
	/*
	 * Getter und Setter
	 */
	String getName(){
		return name;
	}
	
	void setName(String name){
		this.name = name;
	}
	
	long getValue(){
		return value;
	}
	
	/*
	 * Methoden
	 */
	

	
	@Override
	public String toString(){
		return "\n\n Aktienpaket: \n\n   " + "von " + name +",   Wert: " + value;
	}
	

	
}
