package de.B�rsenspiel.AccountManager;

import de.B�rsenspiel.B�rse.Share;

public class Shareitem extends Asset {
	
	/*
	 * Variablen
	 */
	private Share share;
	private long purchaseValue;
	private int amount;
	
	/*
	 * Constructor
	 */
	public Shareitem(){
		
	}
	
	public Shareitem(Share share, int amount){
		this.share = share;
		this.amount = amount;
		this.purchaseValue = (long)(share.getPrice()*amount);
		this.name = share.getName() + "aktien";
		this.value = purchaseValue;						// Werte neu setzten
	}
	
	/*
	 * Getter und Setter
	 */
	public long getPurchaseValue(){
		return purchaseValue;
	}
	
	public int getAmount(){
		return amount;
	}
	
	public Share getShare(){
		return share;
	}
	
	/*
	 * Methoden
	 */
	
	public void changeAmount(Shareitem item, Share share, int aamount){
		this.share = share;
		this.purchaseValue =item.purchaseValue + (share.getPrice()*aamount);
		this.amount = item.amount + aamount;
	}
	
	@Override
	public String toString(){
		return share + ", Menge: " + amount + ", Paketpreis: " + purchaseValue;
	}


}
