package de.Börsenspiel.AccountManager;

import de.Börsenspiel.Börse.StockPriceProvider;
import de.Börsenspiel.Exceptions.NotEnoughtMoney;
import de.Börsenspiel.Exceptions.PlayerNotFound;
import de.Börsenspiel.Exceptions.ShareNotFound;
import de.Börsenspiel.Exceptions.UnableToRemove;

public interface IAccountManager {

	void createPlayer(String name);
	
	void buyShare(String name, String share, int amount);
	void sellShare(String name, String share, int amount); 
	String getOneKonto(String name);
	String getcurrentShareValue(String name);
	void getShareList();
	StockPriceProvider getSPP();
	String playerToString(String name);
	String shareDepositAccountToString(String name);

	
	Player[] getPlayers();

}
