package de.Börsenspiel.AccountManager;

import de.Börsenspiel.Exceptions.NotEnoughtMoney;

public class Bankaccount extends Asset {
	
	/*
	 * Variablen
	 */
	private long currentMoney;
	
	/*
	 * Constructor
	 */
	public Bankaccount(long startmoney){
		this.currentMoney = startmoney;
	}
	
	/*
	 * Getter und Setter
	 */
	public long getMoney(){
		return currentMoney;
	}
	
	/*
	 * Methoden
	 */
	
	public void addValue(long value){
		currentMoney += value;
	}
	
	public void removeValue(long value) throws NotEnoughtMoney{
		if(currentMoney-value < 0){
			throw new NotEnoughtMoney();
		}else{
			currentMoney -= value;
		}
	}
	
	@Override
	public String toString(){
		return "-Kontostand: " + currentMoney + " in Cent";
	}
	

}
