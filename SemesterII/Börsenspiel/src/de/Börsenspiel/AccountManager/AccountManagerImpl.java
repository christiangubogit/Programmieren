package de.Börsenspiel.AccountManager;

import java.util.Arrays;

import de.Börsenspiel.Börse.*;
import de.Börsenspiel.Exceptions.NotEnoughtMoney;
import de.Börsenspiel.Exceptions.PlayerNotFound;
import de.Börsenspiel.Exceptions.ShareNotFound;
import de.Börsenspiel.Exceptions.UnableToRemove;



public class AccountManagerImpl implements IAccountManager {
	/*
	 * Variablen
	 */
	private Player[] player = new Player[0];
	private Player currentPlayer;
	private StockPriceProvider SPP;
	
	

	/*
	 * Constructor
	 */
	public AccountManagerImpl(String name){
		switch("random"){
		case("random"):
			this.SPP = new RandomStockPriceProvider();
			break;
		case("constant"):
			this.SPP = new ConstStockPriceProvider();
			break;
		default: 
			this.SPP = new RandomStockPriceProvider();
		}
		
		
	}
	
	/*
	 * Playermethods
	 */
	public Player[] getPlayers(){
		return player;
	}
	
	private Player searchPlayer(String name) throws PlayerNotFound{
		for(int i = 0; i < player.length; i++){
			if(player[i].getName().equals(name)){
				return player[i];
			}
		}
		throw new PlayerNotFound(name + " nicht gefunden!!!!!");
	}

	@Override
	public void createPlayer(String name) {
		
		/*
		 * Erstellen eines neuen Spieler mit Individuellem Name,Konto und Aktiendepot
		 */
		Player[] buffer = new Player[player.length + 1];
		for(int i = 0; i < player.length; i++){
			buffer[i] = player[i];
		}
		player = buffer;
		
		player[player.length - 1] = new Player(name);
		
	}
	
	public String playerToString(String name){
		String buffer = null; 
			try {
				buffer = searchPlayer(name).toString(SPP.getList());
			} catch (PlayerNotFound e) {
				e.printStackTrace();
			}
			
			return buffer;
	}
	
	
	/*
	 * ShareMethods
	 */

	
	@Override
	public void buyShare(String name, String share, int amount){
		try {
			currentPlayer = searchPlayer(name);
			Share buffer = SPP.getShare(share);
			currentPlayer.buy(buffer, amount);	
		} catch (PlayerNotFound e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotEnoughtMoney e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ShareNotFound e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void sellShare(String name, String share, int amount){
			try {
				currentPlayer = searchPlayer(name);
				Share buffer = SPP.getShare(share);
				currentPlayer.sell(buffer, amount);
						
			} catch (PlayerNotFound e) {
				e.printStackTrace();
			} catch (ShareNotFound e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnableToRemove e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	@Override
	public String shareDepositAccountToString(String name) {
		try {
			currentPlayer = searchPlayer(name);
		} catch (PlayerNotFound e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return currentPlayer.getDepot().toString();
	}
	

	@Override
	public String getOneKonto(String name){
		try {
			currentPlayer = searchPlayer(name);
		} catch (PlayerNotFound e) {
			e.printStackTrace();
		}
		return currentPlayer.getKonto().toString();
		
	}
	
	@Override
	public void getShareList() {
		Arrays.toString(SPP.getList());
	}

	@Override
	public String getcurrentShareValue(String name){
		Share buffer = null;
		try {
			buffer = SPP.getShare(name);
		} catch (ShareNotFound e) {
			e.printStackTrace();
		}
		return buffer.toString();
	}
	
	@Override
	public StockPriceProvider getSPP(){
		return SPP;
	}
	

	@Override
	public String toString() {
		return "AccountManagerImpl [player=" + Arrays.toString(player)
				+ ", list=" + Arrays.toString(SPP.getList()) + ", currentPlayer="
				+ currentPlayer + ", playercounter=" + "]";
	}

}
