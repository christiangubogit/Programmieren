package de.B�rsenspiel.AccountManager;

import java.util.Arrays;

import de.B�rsenspiel.B�rse.Share;
import de.B�rsenspiel.Exceptions.ShareNotFound;
import de.B�rsenspiel.Exceptions.UnableToRemove;

public class ShareDepositAccount extends Asset {
	
	/*
	 * Variablen
	 */
	private Shareitem[] depot = new Shareitem[0];
	
	/*
	 * Getter und Setter
	 */
	public Shareitem[] getDepot(){
		return depot;
	}
	
	public int getLength(){
		return depot.length;
	}
	
	/*
	 * Methoden
	 */
	private void updateDepotValue(){
		value = 0;
		for(int i = 0; i < depot.length; i++){
			value += depot[i].getPurchaseValue();
		}
	}
	
	private void extend(){
		Shareitem[] buffer = new Shareitem[depot.length + 1];
		for(int i = 0; i < depot.length; i++){
			buffer[i] = depot[i];
		}
		depot = buffer;
	}
	
	private void reduce(int numb){
		Shareitem[] buffer = new Shareitem[depot.length - 1];
		for(int i = 0; i < numb; i++){
			buffer[i] = depot[i];
		}
		for(int i = numb+1; i <= buffer.length; i++ ){
			buffer[i-1] = depot[i];
		}
		depot = buffer;
	}
	
	private int search(Share share) throws ShareNotFound{
		for(int i = 0; i < depot.length; i++){
			if(share.equals(depot[i].getShare())){
				return i;
			}
		}
		throw new ShareNotFound();
	}
	
	public void add(Share share, int amount){
		int numb;
		try {
			numb = search(share);
		} catch (ShareNotFound e) {
			extend();
			depot[depot.length-1] = new Shareitem(share, amount);
			updateDepotValue();
			return;
		}
		depot[numb].changeAmount(depot[numb],share, amount);
		updateDepotValue();
	}
	
	public void remove(Share share, int amount) throws UnableToRemove{
		int numb;
		try {
			numb = search(share);
		} catch (ShareNotFound e) {
			System.out.print("\n\n> Sie besitzen keine Aktie von" + share.getName());
			throw new UnableToRemove();
		}
		if(depot[numb].getAmount() < amount){
			System.out.print("\n\n> Sie besitzen nicht gen�gen Aktien von" + share.getName());
			throw new UnableToRemove();
		}else if(depot[numb].getAmount() == amount){
			reduce(numb);
			updateDepotValue();
			return;
		}else{
			depot[numb].changeAmount(depot[numb],share, -amount);
			updateDepotValue();
		}
	}
	
	
	public void update(Share[] list){
		for(int i = 0; i < depot.length; i++){
			for(int j = 0; j < list.length; j++ ){
				if(depot[i].getName().equals(list[i].getName())){
					depot[i].changeAmount(depot[i], list[i], 0);
				}
			}
		}
		
	}
	
	@Override
	public String toString(){
		return "-Aktiendepot: " + Arrays.toString(depot) + "  Gesamtwert: " + this.value;
	}
}
