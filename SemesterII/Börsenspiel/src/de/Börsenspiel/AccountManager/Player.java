package de.Börsenspiel.AccountManager;

import de.Börsenspiel.Börse.Share;
import de.Börsenspiel.Exceptions.NotEnoughtMoney;
import de.Börsenspiel.Exceptions.UnableToRemove;



public class Player {

	/*
	 * Variablen
	 */
	private String name;
	private ShareDepositAccount depot;
	private Bankaccount konto;
	
	/*
	 * Constructor
	 */
	
	public Player(String name){
		this.name = name;
		this.depot = new ShareDepositAccount();
		this.konto = new Bankaccount(2000);
	}
	
	/*
	 * Getter und Setter
	 */
	public String getName() {
		return name;
	}
	
	public Bankaccount getKonto() {
		return konto;
	}

	public ShareDepositAccount getDepot() {
		return depot;
	}
	
	/*
	 * Methoden
	 */
	public void buy(Share share, int amount) throws NotEnoughtMoney{
			konto.removeValue(share.getPrice()* amount);
			depot.add(share, amount);
	}
	
	public void sell(Share share, int amount) throws UnableToRemove{
		depot.remove(share, amount);
		konto.addValue(share.getPrice()*amount);
	}

	
	public String toString(Share[] list){
		this.getDepot().update(list);	
		return "Spielername: " + name + "\n\n Aktiendepot: " + depot + "\n\n Geldkonto: " + konto;
	}
}
